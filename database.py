"""database app"""
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import  Column, Integer, String


SQLALCHEMY_DATABASE_URL = "postgresql://postgres:postgres@localhost:5432/db_app"
engine = create_engine(SQLALCHEMY_DATABASE_URL)


Base = declarative_base()

class Person(Base):
    __tablename__ = "people"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    email = Column(String)

class Wallet(Base):
    __tablename__ = "wallets"

    id = Column(Integer, primary_key=True, index=True)
    address = Column(String)
    owner = Column(String)


SessionLocal = sessionmaker(autoflush=False, bind=engine)
