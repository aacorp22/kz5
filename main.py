"""main app"""
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI, Body
from fastapi.responses import JSONResponse, FileResponse

from database import Wallet, Person, SessionLocal, Base, engine

Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/")
def main():
    return FileResponse("public/index.html")

@app.get("/wallets")
def get_wallets_page():
    return FileResponse("public/wallets.html")

@app.get("/api/users")
def get_people(db: Session = Depends(get_db)):
    return db.query(Person).all()

@app.get("/api/users/{id}")
def get_person(id, db: Session = Depends(get_db)):
    person = db.query(Person).filter(Person.id == id).first()
    if person is None:
        return JSONResponse(status_code=404, content={ "message": "Пользователь не найден"})
    return person

@app.post("/api/users")
def create_person(data = Body(), db: Session = Depends(get_db)):
    person = Person(name=data["name"], email=data["email"])
    db.add(person)
    db.commit()
    db.refresh(person)
    return person

@app.put("/api/users/{id}")
def edit_person(id, data  = Body(), db: Session = Depends(get_db)):
    person = db.query(Person).filter(Person.id == id).first()
    if person is None:
        return JSONResponse(status_code=404, content={ "message": "Пользователь не найден"})
    person.email = data["email"]
    person.name = data["name"]
    db.commit()
    db.refresh(person)
    return person

@app.delete("/api/users/{id}")
def delete_person(id, db: Session = Depends(get_db)):
    person = db.query(Person).filter(Person.id == id).first()
    if person is None:
        return JSONResponse( status_code=404, content={ "message": "Пользователь не найден"})
    db.delete(person)
    db.commit()
    return person

@app.get("/api/wallets")
def get_wallets(db: Session = Depends(get_db)):
    return db.query(Wallet).all()

@app.get("/api/wallets/{id}")
def get_wallet(id, db:Session = Depends(get_db)):
    wallet = db.query(Wallet).filter(Wallet.id == id).first()
    if wallet is None:
        return JSONResponse(status_code=404, content={ "message": "Кошелек не найден"})
    return wallet

@app.post("/api/wallets")
def create_wallet(data = Body(), db: Session = Depends(get_db)):
    wallet = Wallet(address=data["address"], owner=data["owner"])
    db.add(wallet)
    db.commit()
    db.refresh(wallet)
    return wallet

@app.put("/api/wallets/{id}")
def edit_wallet(id, data = Body(), db: Session = Depends(get_db)):
    wallet = db.query(Wallet).filter(Wallet.id == id).first()
    if wallet is None:
        return JSONResponse(status_code=404, content={ "message": "Кошелек не найден"})
    wallet.address = data["address"]
    wallet.owner = data["owner"]
    db.commit()
    db.refresh(wallet)
    return wallet

@app.delete("/api/wallets/{id}")
def delete_wallet(id, db: Session = Depends(get_db)):
    wallet = db.query(Wallet).filter(Wallet.id == id).first()
    if wallet is None:
        return JSONResponse(status_code=404, content={ "message": "Кошелек не найден"})
    db.delete(wallet)
    db.commit()
    return wallet
